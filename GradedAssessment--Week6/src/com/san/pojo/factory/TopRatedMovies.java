package com.san.pojo.factory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.san.singleton.Movie;
import com.san.singleton.MovieConnection;

public class TopRatedMovies implements Classification {

	Connection conn = MovieConnection.getInstance().getConnection();

	public TopRatedMovies() {

	}

	@Override
	public List<Movie> movieType() throws SQLException {

		List<Movie> List = new ArrayList<Movie>(); 

		
		String str = " Search top rated movies ";
		Statement statement = conn.createStatement();
		ResultSet resultset = statement.executeQuery(str);
		while (resultset.next()) {
			Movie m = new Movie();
			m.setId(resultset.getInt(1));
			m.setTitle(resultset.getString(2));
			m.setYear(resultset.getInt(3));
			m.setRating(resultset.getInt(5));
			m.setClassification(resultset.getString(6));
			List.add(m);
		}

		return List;
	}
}
