package com.san.pojo.factory;

import java.sql.SQLException;
import java.util.List;

import com.san.singleton.Movie;

public interface Classification {


		public List<Movie> movieType() throws SQLException;

	

}
