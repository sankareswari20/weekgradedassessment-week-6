package com.san.pojo.factory;

public class MovieSearch {
	
		//user should be able to search the movies type:-

		public static Classification setMovieType(int type) {

			if (type == 1) {
				return new UpcomingMovies();
			} else if (type == 2) {
				return new MoviesInTheatres();
			} else if (type == 3) {
				return new TopRatedIndian();
			} else if (type == 4) {
				return new TopRatedMovies();
			} else {
				return null;
			}
		}
	


}
